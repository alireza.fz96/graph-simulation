function myNode(name) {
	this.name = name
	this.children = []

	this.addChild = function (child) {
		this.children.push(child)
	}
	this.changeName = function (name) {
		this.name = name
	}
}

var nodeList = []

function createTreeCopy(node) {
	var newNode = new myNode(node.name)
	for (let child of node.children) newNode.addChild(createTreeCopy(child))
	return newNode
}

function createTreesCopy() {
	let trees = []
	for (let rootNode of nodeList) trees.push(createTreeCopy(rootNode))
	return trees
}

function renderNodes(node) {
	var nodeContainer = createNode(node)
	for (let child of node.children)
		nodeContainer.appendChild(renderNodes(child))

	return nodeContainer
}

function createNode(node) {
	var nodeToolbar = document.createElement('div')
	nodeToolbar.className = 'node__toolbar'

	var addBtn = document.createElement('span')
	addBtn.innerText = '+'
	addBtn.onclick = () => onAddNode(node)

	var removeBtn = document.createElement('span')
	removeBtn.innerText = '-'
	removeBtn.onclick = () => onRemoveNode(node)

	var editBtn = document.createElement('span')
	editBtn.innerText = 'edit'
	editBtn.onclick = () => onEditNode(node)

	nodeToolbar.append(addBtn, removeBtn, editBtn)

	var nodeName = document.createElement('p')
	nodeName.className = 'node__name'
	nodeName.innerText = node.name

	var nodeContainer = document.createElement('div')
	nodeContainer.className = 'node'
	nodeContainer.style.backgroundColor = getRandomColor()

	nodeContainer.append(nodeToolbar, nodeName)
	return nodeContainer
}

function onAddNode(node) {
	var name = prompt('Name: ')
	if (name) {
		let oldTrees = createTreesCopy()
		node.addChild(new myNode(name))
		applyChange(oldTrees, nodeList)
	}
}

function onEditNode(node) {
	var name = prompt('Name: ')
	if (name) {
		let oldTrees = createTreesCopy()
		node.changeName(name)
		applyChange(oldTrees, nodeList)
	}
}

function onRemoveNode(node) {
	let oldTrees = createTreesCopy()
	node.changeName(null)
	applyChange(oldTrees, nodeList)
	for (let index in nodeList) {
		if (node === nodeList[index]) {
			nodeList = nodeList.filter(nodeItem => nodeItem !== node)
			rootElements.splice(index, 1)
		} else removeNode(nodeList[index], node)
	}
}

function removeNode(root, node) {
	root.children = root.children.filter(childNode => childNode !== node)
	for (let child of root.children) {
		removeNode(child, node)
	}
}

function addElement() {
	var name = prompt('Name: ')
	if (name) {
		let newNode = new myNode(name)
		nodeList.push(newNode)
		render(newNode)
	}
}

function diff(oldNode, newNode) {
	if (!newNode || !newNode.name) {
		//if node has been removed
		return node => {
			node.remove()
			return undefined
		}
	} else if (newNode.name !== oldNode.name)
		//if node has been changed
		return node => {
			const newTree = renderNodes(newNode)
			node.replaceWith(newTree)
			return newTree
		}
	else {
		return node => {
			const patchChildren = diffChildren(
				oldNode.children,
				newNode.children
			)
			patchChildren(node)
			return node
		}
	}
}

function diffChildren(oldChildren, newChildren) {
	const childPatches = []
	oldChildren.forEach((oldChild, i) =>
		childPatches.push(diff(oldChild, newChildren[i]))
	)

	// for new children
	const additionalPatches = []
	for (const additionalChild of newChildren.slice(oldChildren.length)) {
		additionalPatches.push(node => {
			node.appendChild(renderNodes(additionalChild))
			return node
		})
	}

	return parent => {
		let i = 0
		for (let [_, child] of parent.childNodes.entries()) {
			if (child.className === 'node') {
				childPatches[i](child)
				i++
			}
		}
		for (patch of additionalPatches) {
			patch(parent)
		}
		return parent
	}
}

var rootElements = []

function applyChange(oldTrees, newTrees) {
	for (let index in oldTrees) {
		const patch = diff(oldTrees[index], newTrees[index])
		rootElements[index] = patch(rootElements[index])
	}
}

function render(node) {
	let renderedNodes = renderNodes(node)
	rootElements.push(renderedNodes)
	document.querySelector('#elements').appendChild(renderedNodes)
}

function getRandomColor() {
	var letters = '0123456789ABCDEF'
	var color = '#'
	for (var i = 0; i < 6; i++) {
		color += letters[Math.floor(Math.random() * 16)]
	}
	return color
}
